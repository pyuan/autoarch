#!/bin/bash
useradd -m guest
passwd guest
curl -O https://gitlab.com/pyuan/autoarch/-/raw/master/config.sh
curl -O https://gitlab.com/pyuan/autoarch/-/raw/master/variables.sh
curl -O https://gitlab.com/pyuan/autoarch/-/raw/master/packages.txt
mv config.sh /home/guest
mv variables.sh /home/guest
echo "guest ALL=(ALL) ALL" >> /etc/sudoers
pacman -S --needed - < packages.txt --noconfirm
systemctl enable --now NetworkManager
su -c "/bin/sh /home/guest/config.sh" - guest
