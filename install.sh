#!/bin/bash

curl -O https://gitlab.com/pyuan/autoarch/-/raw/master/variables.sh
source ./variables.sh
echo "the main linux partition must be the first partition"
echo "this will ask for a root passwd"
echo "Would you like to begin arch installation? (Y/N)"
read install_yn

if [[ $install_yn == "n" ]] || [[ $install_yn == "N" ]]; then 
	exit 0 

elif [[ $install_yn == "y" ]] || [[ $install_yn == "Y" ]]; then
	timedatectl set-ntp true
	mkfs.ext4 /dev/$drive1
	mount /dev/$drive1 /mnt
	curl -O $mirrorlist
	mv mirrorlist /etc/pacman.d/mirrorlist
	pacstrap /mnt $pacstrap
	genfstab -U /mnt >> /etc/fstab
	cp ./chroot.sh /mnt
	cp ./variables.sh /mnt
	curl -O https://gitlab.com/pyuan/autoarch/-/raw/master/chroot.sh
	arch-chroot /mnt sh chroot.sh
else
	echo "Not a command"
	exit 0
fi
