#!/bin/bash 

# add option in variables so that these are already set
source $HOME/variables.sh
echo " you have chosen \
vimplug:$vimplug_yn deoplete:$deoplete_yn nvim_plugins:$nvim_plugins_yn \
font:$font_yn icon:$icon_yn nvidia/xrandr:$xrandr_yn \
wallpaper/xinit:$wallpaper_yn restart:$restart_yn"

#echo "Would you like to continute? (Y/N)"
#read userinput

if [[ $userinput == "n" ]] || [[ $userinput == "N" ]]; then 
	exit 0 

elif [[ $userinput == "y" ]] || [[ $userinput == "Y" ]]; then

	# files in .config
	curl -O $configrepo 

	if [[ $configrepo =~ ([^/.]+)([.]zip)$ ]]; then 
		unzip ${BASH_REMATCH[1]} 
		configname=${BASH_REMATCH[1]}
	fi
	# move config files to fakehome, rename to $HOME when in production
	mv $configname/.config $HOME
	mv $configname/.local $HOME
	mv $configname/.xinitrc $HOME
	mv $configname/.zshenv $HOME

	# cleanup
	rm -r $configname ${BASH_REMATCH[1]}.zip

	# download vimplug for neovim
	if [[ $vimplug_yn == "n" ]] || [[ $vimplug_yn == "N" ]]; then 
		: 
	elif [[ $vimplug_yn == "y" ]] || [[ $vimplug_yn == "Y" ]]; then
		curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
		https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	else
		echo "Not a command"
		:
	fi

	# install deoplete dependency 
	if [[ $deoplete_yn == "n" ]] || [[ $deoplete_yn == "N" ]]; then 
		: 
	elif [[ $deoplete_yn == "y" ]] || [[ $deoplete_yn == "Y" ]]; then
		pip3 install --user pynvim
	else
		echo "Not a command"
		:
	fi

	# install plugins
	if [[ $nvim_plugins_yn == "n" ]] || [[ $nvim_plugins_yn == "N" ]]; then 
		: 
	elif [[ $nvim_plugins_yn == "y" ]] || [[ $nvim_plugins_yn == "Y" ]]; then
		nvim ~/.config/nvim/init.vim -c ":PlugInstall" -c ":qa"
	else
		echo "Not a command"
		:
	fi
	
	# material design fonts
	if [[ $icon_yn == "n" ]] || [[ $icon_yn == "N" ]]; then 
		: 
	elif [[ $icon_yn == "y" ]] || [[ $icon_yn == "Y" ]]; then
		if [[ $font =~ ([^/]+)$ ]]; then
		curl -fLo ~/.local/share/fonts/${BASH_REMATCH[1]} --create-dirs \
			$font
		fi
	else
		echo "Not a command"
		:
	fi

	# icons 
	if [[ $icon_yn == "n" ]] || [[ $icon_yn == "N" ]]; then 
		: 
	elif [[ $icon_yn == "y" ]] || [[ $icon_yn == "Y" ]]; then
		curl -O $icon 
		if [[ $icon =~ ([^/.]+)([.]zip)$ ]]; then 
			unzip -q ${BASH_REMATCH[1]}
			icon_name=${BASH_REMATCH[1]}
		fi
		mkdir -p $HOME/.local/share/icons 
		mv $icon_name/Nordic $HOME/.local/share/icons
		mv $icon_name/Zafiro-icons $HOME/.local/share/icons
		rm -r $icon_name ${BASH_REMATCH[1]}.zip
	else
		echo "Not a command"
		:
	fi

	# xrandr and nvidia
	if [[ $xrandr_yn == "n" ]] || [[ $xrandr_yn == "N" ]]; then 
		: 
	elif [[ $xrandr_yn == "y" ]] || [[ $xrandr_yn == "Y" ]]; then
		echo "$nvidia" >> $HOME/.xinitrc
	else
		echo "Not a command"
		:
	fi

	# download wallpaper, append xwallpaper and other xinit items to .xinitrc
	if [[ $wallpaper_yn == "n" ]] || [[ $wallpaper_yn == "N" ]]; then 
		: 
	elif [[ $wallpaper_yn == "y" ]] || [[ $wallpaper_yn == "Y" ]]; then
		if [[ $wallpaper =~ ([^/]+)$ ]]; then
			curl -fLo $HOME/.config/wallpapers/${BASH_REMATCH[1]} --create-dirs \
				$wallpaper 
			echo "xwallpaper --zoom $HOME/.config/wallpapers/${BASH_REMATCH[1]}" >> \
			$HOME/.xinitrc 
			echo "$other_xinit" >> $HOME/.xinitrc
		fi
	else
		echo "Not a command"
		:
	fi
	echo "finished"

else
	echo "Not a command"
	:
fi 
