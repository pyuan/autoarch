#!/bin/bash

# install options
userinput="y"

# drive type (sda, vda, etc)
drive="vda"
drive1="vda1"

# essential packages you want
pacstrap="base linux linux-firmware networkmanager vim amd-ucode grub"

# mirror list for downloading packages (url)
mirrorlist="https://gitlab.com/pyuan/autoarch/-/raw/master/mirrorlist"

# region put in "Region/City" format
region="America/New_York"

# your localization
locale="en_US.UTF-8 UTF-8"

lang="LANG=en_US.UTF-8"

hostname="mypc"

hosts="127.0.0.1\tlocalhost
::1\t\t\tlocalhost
127.0.1.1\t$hostname.localdomain\t$hostname"

# options

# install vimplug?
vimplug_yn="y"

# install pynvim for deoplete?
deoplete_yn="y"

# install plugins in ~/.config/nvim/init.vim?
nvim_plugins_yn="y"

# download fonts?
font_yn="y"

# dowload icons?
icon_yn="y"

# do you have nvidia or xrandr settings to append to .xinitrc?
xrandr_yn="n"

# do you want to dowload a wallpaper?
wallpaper_yn="y"

# put the https download link here
configrepo="https://gitlab.com/pyuan/nord-dots/-/archive/master/nord-dots-master.zip"

# wallpaper download link
wallpaper="https://gitlab.com/pyuan/wallpapers/-/raw/master/sea.png"

# font download link
font="https://github.com/Templarian/MaterialDesign-Font/raw/master/MaterialDesignIconsDesktop.ttf"

# icon dowload link
icon="https://gitlab.com/pyuan/nordic-zafiro/-/archive/master/nordic-zafiro-master.zip"

# nvidia and xrandr settings (write your own)
nvidia=$'nvidia-settings --assign CurrentMetaMode="DP-4: nvidia-auto-select +0+0 {ForceFullCompositionPipeline=On}, HDMI-0: nvidia-auto-select +2560+0 {ForceCompositionPipeline=On}"
\nxrandr --output DP-4 --mode 2560x1440 --rate 165
xrandr --output HDMI-0 --rotate "right"'

# other items in your xinit (write your own)
other_xinit=$"
polybar -r nord &
setxkbmap -option caps:escape
picom &
exec bspwm
"
