#!/bin/bash
source ./variables.sh
ln -sf /usr/share/zoneinfo/$region /etc/localtime
hwclock --systohc
echo $locale >> /etc/locale.gen
locale-gen
echo $lang >> /etc/locale.conf
echo $hostname >> /etc/hostname
echo $hosts >> /etc/hosts
grub-install --target=i386-pc /dev/$drive
grub-mkconfig -o /boot/grub/grub.cfg
passwd
exit
