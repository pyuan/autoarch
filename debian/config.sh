#!/bin/bash
echo 'export TERM=linux' >> ~/.bash_profile
curl -O https://gitlab.com/pyuan/autoarch/-/raw/master/debian/.bashrc
curl -O https://gitlab.com/pyuan/autoarch/-/raw/master/debian/.vimrc
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
vim -c ":PlugInstall" -c ":qa"
clear
bash
